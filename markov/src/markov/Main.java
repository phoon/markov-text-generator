package markov;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import java.util.Vector;

public class Main {
	public static void main(String[] args){
		System.out.print("Insert absolute path to book:\n");
		Scanner sc = new Scanner(System.in);
		String file = new String(sc.next());
		System.out.print("Enter amount of words to generate:\n");
		int reps = sc.nextInt();
		Vector fileContents = new Vector();
		String lastWord = new String();
		String tmpsen = new String();
		
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		        Vector tmp = new Vector();
		        if (line != null){
		        	for (int i = 0; i <= line.length(); i++){
			        	try {
			        		if (line.charAt(i) != ' '){
			        			tmp.add(line.charAt(i));
			        		}
			        		
			        	} catch (Exception e){
			        		if (line.contains(" ")){
			        			String[] tmp2 = line.split(" ");
			        			for (String a : tmp2){
			        				fileContents.add(a);
			        			}
			        			break;
			        		}
			        		fileContents.add(line);
			        	}
			        }
		        }
		    }
		    String everything = sb.toString();
		} catch (IOException e) {
			System.out.println("File: "+file+" not found!");
		}
		
		Vector<Vector<String>> markovChain = new Vector<Vector<String>>();
		Vector<String> tmp = new Vector<>();
		int j = 0;
		for (j = 0; j < fileContents.size(); j+=1){
			tmp.add(fileContents.get(j).toString());
			if (j != fileContents.size()-1){
				tmp.add(fileContents.get(j+1).toString());
			}
			markovChain.add(tmp);
			tmp = new Vector<>();
		}
		for (int i = 0; i < markovChain.size()-2; i++){
			tmp.add(markovChain.get(i+1).toString());
			markovChain.get(i).addAll(tmp);
			tmp = new Vector<>();
		}
		long state = Math.round(Math.random()*fileContents.size());
		String kWord = markovChain.get((int)state).get(0);
		String kWord1 = markovChain.get((int)state).get(1);
		String kWord2 = kWord + " " + kWord1;
		//System.out.println(kWord + " " + kWord1);
		Vector<Integer> occurances = new Vector<Integer>();
		//System.out.println(fileContents);
		for (int i = 0; i < fileContents.size(); i++){
			String tmpStr = fileContents.get(i).toString();
			String tmpStr1 = "";
			if (i != fileContents.size()-1){
				tmpStr1 = fileContents.get(i+1).toString();
			}
			if (tmpStr.toString().equals(kWord.toString()) && tmpStr1.toString().equals(kWord1.toString())){
				occurances.add(i);
			}
		}
		occurances.removeAll(Collections.singleton(-1));
		long transformation = (long)Math.floor(Math.random()*occurances.size());
		String sen = kWord2 + " " + fileContents.get((occurances.get((int)transformation)+2)).toString();

		
		//System.out.println(sen);
		
		for (j = 0; j < reps; j++){
		occurances.clear();
		kWord = sen.split(" ")[sen.split(" ").length-2];
		kWord1 = sen.split(" ")[sen.split(" ").length-1];
		for (int i = 0; i < fileContents.size(); i++){
			String tmpStr = fileContents.get(i).toString();
			String tmpStr1 = "";
			if (i != fileContents.size()-1){
				tmpStr1 = fileContents.get(i+1).toString();
			}
			if (tmpStr.toString().equals(kWord.toString()) && tmpStr1.toString().equals(kWord1.toString())){
				occurances.add(i);
			}
		}
		for (int a : occurances){
			//System.out.println(fileContents.get(a+2));
		}
		occurances.removeAll(Collections.singleton(-1));
		transformation = (long)Math.floor(Math.random()*occurances.size());
		sen = sen + " " + fileContents.get((occurances.get((int)transformation)+2)).toString();
		sen = sen.trim();
		if (sen.equals(tmpsen)){
			break;
		}
		tmpsen = sen;
		//System.out.println(sen);
		}
		System.out.println(sen);
		occurances.removeAll(Collections.singleton(-1));
	}
}
